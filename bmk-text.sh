#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
if [ "$#" -eq 1 ]; then
	echo "Adding bookmarks from file $1 to database!"
	# Pre-append the script name used to add a bookmark entry to every line of the file
	sed -e 's/^/.\/add-bmk.sh /' $1 > temp_bmk.sh
	chmod u+x temp_bmk.sh
	./temp_bmk.sh
else
	echo "Error! Expecting filepath/name containing the tags/bookmark!"
fi


