#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
# Create simlink to point to user's directory
BUKUPATH=$HOME/.local/share/buku
DBPATH=$HOME/buku
echo "Creating simlink to $BUKUPATH/bookmarks.db"
mkdir $BUKUPATH
ln -s $DBPATH/bookmarks.db $BUKUPATH/bookmarks.db
