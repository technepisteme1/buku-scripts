#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
# Search for entries under following tags
# Separate tags with commas for OR, + for AND
buku --stag $1
