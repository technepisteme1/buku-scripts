This is a repo for my basic (hacky) shell scripts I use for accessing certain features of the Buku bookmark manager.


# Background:
----------------------------
Original Buku bookmark manager project can be found here:
https://github.com/jarun/Buku
Please read the man pages for Buku, as there are a lot features available!

Buku has been a very good tool to use so far, and given that it is designed to work in the terminal, using scripts to simplify some commands (or produce different behaviour) is quite natural.
I store my bookmarks in a local git repo, and instead of command aliases just have these shell scripts to help me with adding my existing backlog of URLs scattered about on different machines.

The scripts have been given names such that they don't share the first couple of chars, thus allowing tab auto-complete to start working after typing two chars or fewer.

These scripts have only been tested on linux distros (arch, devuan), however should work on POSIX OSs that have Buku.

_Dependencies:_

- Buku 4.1 or higher
- wc (GNU coreutils 8.30 or higher)

# Scripts:
----------------------------

add-bmk.sh
----------------------------
Add a bookmark to the Buku database file.<br>
There are two input arguments: the tags for the URL to be added separated by commas (no spaces), and the URL itself.<br>
(Make sure to enclose the URL in quotes if it contains ampersands, semicolons, and any other characters used by the shell.)<br>
Only the "-a" flag is passed to Buku ("--suggest" in my case is too much hassle as I have 100s of tags).<br>

Example:<br>
>    $ ./add-bmk.sh git,main https://gitlab.com/

Output:<br>
>    Passing bookmark and tags to Buku!<br>
>    764. Iterate faster, innovate together | GitLab<br>
>    \> https://gitlab.com/<br>
>    \+ Our open DevOps platform is a single application for unparalleled collaboration, visibility, and development velocity.<br>
>    \# git,main<br>



replace-tag.sh
----------------------------
Replace an old tag for a new one, or deleting an old tag.<br>
There are two input arguments: the old tag to replace or delete, and the new tag.<br>
The new tag can be ommited if you wish to delete the old tag all together (must be manually confirmed!).<br>
Only the "--replace" flag is passed to Buku.<br>

Example (replace tag):<br>
>    $ ./replace-tag.sh development devops<br>

Output:<br>
> Replacing tag development with tag devops!<br>
> Index 764 updated<br>


Example (delete tag):<br>
>    $ ./replace-tag.sh devops<br>

Output:<br>
> Deleting tag devops!<br>
> Delete the tag(s) from ALL bookmarks? (y/n): y<br>
> 1 record(s) updated<br>



## Scripts relying on Buku's "-u" (update entry) flag:
----------------------------

atag.sh
----------------------------
Add a tag (or tags) to an existing Buku entry.<br>
There are two input arguments: the URL id number (this is displayed after first adding the entry), and the extra tag/s to be added (comma separated).<br>
Flags "-u" (update entry), "--tag" (modify tags), and "+" (add following tags) are passed to Buku.<br>

Example:<br>
> $ ./atag.sh 764 development<br>

Output:<br>
> Adding tag development to entry number 764!<br>
> 764. Iterate faster, innovate together | GitLab<br>
>    \> https://gitlab.com/<br>
>    \+ Our open DevOps platform is a single application for unparalleled collaboration, visibility, and development velocity.<br> 
>    \# development,git,main<br>



immutable-set.sh
----------------------------
Set Buku entry to be immutable (prevent auto-updates to URL, title, comment).<br>
There only input argument is the URL id number.<br>
Flags "-u", and "--immutable 1" (set immutable) are passed to Buku.<br>
A lot of URLs will eventually go offline, however I still want them archived. (Although there is an option in Buku to get an archived version I think.)<br>

Example:<br>
> $ ./immutable-set.sh 764<br>

Output:<br>
> 764. Iterate faster, innovate together | GitLab (L)<br>
>    \> https://gitlab.com/<br>
>    \+ Our open DevOps platform is a single application for unparalleled collaboration, visibility, and development velocity.<br>
>    \# development,git,main<br>



title-change.sh
----------------------------
Change the title of an existing Buku entry.<br> 
Sometimes titles cannot be grabbed automatically by Buku (or too long, unusual chars etc.).<br>
There are two input arguments: the URL id number (this is displayed after first adding the entry), and the new title in double quotes.<br>
Flags "-u", "--title" (set title) are passed to Buku.

Example:<br>
> $ ./title-change.sh 764 "GitLab main site"<br>

Output:<br>
> Change bookmark number 764 title to GitLab main site!<br>
> 764. GitLab main site (L)<br>
>    \> https://gitlab.com/<br>
>    \+ Our open DevOps platform is a single application for unparalleled collaboration, visibility, and development velocity.<br>
>    \# development,git,main<br>



## Scripts extending Buku functions:
----------------------------

count-bmk.sh
----------------------------
Count the number of Buku entries and return the total.<br>
No arguments expected.<br>
New method uses Buku's built-in formatting "-f" flag to only print one line per entry, now no need for grep!<br>
The stream from Buku is piped into wordcount (wc) with "-l" flag that counts the number of lines.<br>

The documentation of Buku has not mentioned any flags that return the total entries, so I'm using this hack for the moment. Later I will probably check the Python code and see if this feature could be added. As the database is SQLite, I'm sure that Python could easily count the database entries.<br>

Example:<br>
> $ ./count-bmk.sh <br>

Output:<br>
> 764 bookmarks saved.<br>



# License:
----------------------------
The scripts are licensed under the GNU Public License v3 (GPLv3) and SPDX identifiers have been used to reduce notice sizes in each script file. More can be found out about the SPDX here:<br>
https://spdx.dev/ids/

You can use grep to get the license identifier:<br>
> $ grep "SPDX" add-bmk.sh <br>

Output:<br>
> \# SPDX-License-Identifier: GPL-3.0-only<br>


## DISCLAIMER:
----------------------------
I am NOT an expert on Buku, and there may be better ways of implementing the functionality I have.

Copyright (C) 2021  Andrey Miroshnikov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

