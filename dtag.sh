#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
if [ "$#" -eq 2 ]; then
	echo "Removing tag $2 from entry number $1!"
	buku -u $1 --tag - $2
else
	echo "Error! Expecting bookmark index as first arg, tag/s (comma separated) as second arg!"
fi
