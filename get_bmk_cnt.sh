#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
# Count the number of bookmarks and return string of the total

#COUNT=$(buku -p | grep -E '96m[0-9]+\.' | wc -l) # Old method used regex to match formatting
#COUNT=$(buku -p -f 1 | wc -l) # 2nd method, while more efficient, still wasteful

# New (third) method which only takes the last entry (whose number matches the total)
# and uses regex with sed to extract the number.
buku -p -1 -f 1 | sed 's/[ \t]*\([0-9]\{1,\}\).*/\1/'
