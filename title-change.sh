#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
if [ "$#" -eq 2 ]; then
	echo "Change bookmark number $1 title to $2!"
	buku -u $1 --title $2
else
	echo "Error! Expecting bookmark index as first arg, new title as second!"
fi
