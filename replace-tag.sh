#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only

if [ "$#" -eq 2 ]; then
	echo "Replacing tag $1 with tag $2!"
	buku --replace $1 $2
elif [ "$#" -eq 1 ]; then
	echo "Deleting tag $1!"
	buku --replace $1
else
	echo "Error! Expecting old tag as first arg, new tag as second (optional)!"
fi
