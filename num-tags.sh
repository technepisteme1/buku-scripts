#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
# Count the number of bookmark tag groups in the database using wordcount

# New method uses Buku's built-in formatting to only print one line per entry, still needs to subtract one for ane extra empty line!
COUNT=$(buku -t --np | wc -l)
echo "${COUNT}-1 tags used"
