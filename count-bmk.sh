#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
# Print number corresponding to total number of bookmarks.

echo "$(./get_bmk_cnt.sh) bookmarks saved."
