#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
# Add a bookmark to buku, tags come as first arg
if [ "$#" -eq 2 ]; then
	echo "Passing bookmark and tags to Buku!"
	buku -a $2 $1 --immutable 1 #buku --suggest -a $2 $1 
else
	echo "Error! Expecting comma-separated tags as first arg, site URL as second arg!"
fi
