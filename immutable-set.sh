#!/bin/sh
# Copyright (C) 2021 Andrey Miroshnikov
# SPDX-License-Identifier: GPL-3.0-only
# Make given bookmark entry immutable
buku -u $1 --immutable 1
